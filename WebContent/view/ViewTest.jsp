<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>TomcatTest</title>
	</head>

	<body>
		<h1>Favorite Works</h1>
		<ul>
		<c:forEach var="i" begin="0" end="${fn:length(works)-1}">
			<li>${works[i]}</li>
		</c:forEach>
		</ul>
	</body>
</html>