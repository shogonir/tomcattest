<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>TomcatTest</title>
	</head>

	<body>
		<h1>関東</h1>
		<ul>
		<c:forEach var="i" begin="0" end="${fn:length(prefs)-1}">
			<li><a href="${prefs[i].link}">${prefs[i].name}</a></li>
		</c:forEach>
		</ul>
	</body>
</html>