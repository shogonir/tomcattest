package model;

import java.io.Serializable;
import java.util.List;

import classes.Topic;

public class BbsModel implements Serializable {

	private List<Topic> threads;

	public List<Topic> getThreads() {
		return threads;
	}

	public void setThreads(List<Topic> threads) {
		this.threads = threads;
	}
}
