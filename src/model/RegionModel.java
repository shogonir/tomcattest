package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import classes.Prefecture;

public class RegionModel implements Serializable {

	List<Prefecture> prefectures = new ArrayList<>();

	public void addPrefecture (Prefecture pref) {
		this.prefectures.add(pref);
	}

	public List<Prefecture> getPrefectures () {
		return this.prefectures;
	}
}
