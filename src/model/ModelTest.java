package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ModelTest implements Serializable {

	private String name;
	private List<String> works;

	public ModelTest () {
		works = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addWork (String work) {
		this.works.add(work);
	}

	public List<String> getWorks () {
		return this.works;
	}
}
