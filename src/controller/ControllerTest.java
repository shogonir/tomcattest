package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.ModelTest;

@WebServlet("/test")
public class ControllerTest extends HttpServlet {

	public void doGet (HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

		ModelTest model = new ModelTest();
		model.setName("shogonir");
		model.addWork("SummerWars");
		model.addWork("Inception");

		req.setAttribute("name", model.getName());
		req.setAttribute("works", model.getWorks());

		RequestDispatcher dispatcher = req.getRequestDispatcher("view/ViewTest.jsp");
		dispatcher.forward(req, res);
	}
}
