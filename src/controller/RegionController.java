package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.RegionModel;
import classes.Prefecture;

@WebServlet("/region")
public class RegionController extends HttpServlet {

	public void doGet (HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		RegionModel region = new RegionModel();
		String base_url = "https://ja.wikipedia.org/wiki/";
		region.addPrefecture(new Prefecture("東京", base_url + "東京都"));
		region.addPrefecture(new Prefecture("埼玉", base_url + "埼玉県"));
		region.addPrefecture(new Prefecture("千葉", base_url + "千葉県"));
		region.addPrefecture(new Prefecture("栃木", base_url + "栃木県"));
		region.addPrefecture(new Prefecture("茨城", base_url + "茨城県"));
		region.addPrefecture(new Prefecture("群馬", base_url + "群馬県"));
		region.addPrefecture(new Prefecture("神奈川", base_url + "神奈川県"));

		req.setAttribute("regionName", "関東");
		req.setAttribute("prefs", region.getPrefectures());
		RequestDispatcher dispatcher = req.getRequestDispatcher("view/RegionView.jsp");
		dispatcher.forward(req, res);
	}
}
