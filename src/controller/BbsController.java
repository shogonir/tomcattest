package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/bbs")
public class BbsController extends HttpServlet {

	public void doGet (HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setAttribute("var", "BBS");
		RequestDispatcher dispatcher = req.getRequestDispatcher("view/bbs/BbsView.jsp");
		dispatcher.forward(req, res);
	}
}
